FROM jorgealexandro/wso2ei-integrator:6.2.0

ADD --chown=wso2carbon:wso2 https://s3-us-west-2.amazonaws.com/nst-itsol/ebb-prod/mssql-jdbc-6.4.0.jre8.jar ${WSO2_SERVER_HOME}/lib

# expose integrator ports
EXPOSE 8280 8243 9443 4100

# set entrypoint to init script
ENTRYPOINT ["/home/wso2carbon/init.sh"]